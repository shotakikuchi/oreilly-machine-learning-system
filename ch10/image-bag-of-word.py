import mahotas as mh
import numpy as np
import pprint
from matplotlib import pyplot as plt
from glob import glob
from mahotas.features import surf
import pickle
import os, sys
from sklearn.cluster import KMeans


def get_all_descriptors():
  basedir = 'ch10sampleImages'

  images = glob('../{}/*.jpg'.format(basedir))
  all_descriptors = []

  for im in images:
    im = mh.imread(im, as_grey=True)
    im = im.astype(np.uint8)
    all_descriptors.append(surf.surf(im, descriptor_only=True))

  with open('all_descriptors.pickle', mode='wb') as f:
    pickle.dump(all_descriptors, f)


### all_descriptors
if os.path.isfile("all_descriptors.pickle"):
  with open("all_descriptors.pickle", mode="rb") as f:
    all_descriptors = pickle.load(f)

else:
  all_descriptors = get_all_descriptors()

### concatenated
if os.path.isfile("concatenated.pickle"):
  with open("concatenated.pickle", mode="rb") as f:
    concatenated = pickle.load(f)

else:
  # すべての記述子を一つの配列にする
  concatenated = np.concatenate(all_descriptors)
  # 32番目ごとのベクトルだけを使用する
  concatenated = concatenated[::32]
  with open('concatenated.pickle', mode='wb') as f:
    pickle.dump(concatenated, f)

k = 256
if os.path.exists("km_fited.pickle"):
  with open("km_fited.pickle", mode="rb") as f:
    km = pickle.load(f)
else:
  km = KMeans(k)
  km.fit(concatenated)
  with open('km_fited.pickle', mode='wb') as f:
    pickle.dump(km, f)

### features
if os.path.exists("features.pickle"):
  with open("features.pickle", mode="rb") as f:
    features = pickle.load(f)
else:
  features = []
  for d in all_descriptors:
    c = km.predict(d)
    features.append(np.array([np.sum(c == ci) for ci in range(k)]))

  features = np.array(features)
  with open('features.pickle', mode='wb') as f:
    pickle.dump(features, f)

print(features.shape)
