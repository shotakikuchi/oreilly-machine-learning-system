genre_list = ["blues", "classical", "country", "disco", "hiphop", "jazz", "metal", "pop", "reggae", "rock"]
# genre = "blues"
for genre in genre_list:
  filename = '../command/%s.txt' % genre
  f = open(filename, 'w')
  for i in range(10):
    a = "sox %s.0000%d.au %s.0000%d.wav\t\n" % (genre, i, genre, i)
    f.writelines(a)

  for i in range(10, 100):
    a = "sox %s.000%d.au %s.000%d.wav\t\n" % (genre, i, genre, i)
    f.writelines(a)
  f.close()
