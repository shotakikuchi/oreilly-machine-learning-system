import sys
import os
import glob

import numpy as np
import scipy
import scipy.io.wavfile

from collections import defaultdict
from matplotlib import pylab

from sklearn.metrics import precision_recall_curve, roc_curve
from sklearn.metrics import auc
from sklearn.cross_validation import ShuffleSplit

import matplotlib.pyplot as plt
from matplotlib.ticker import EngFormatter
from sklearn.metrics import confusion_matrix

CHART_DIR = os.path.join("..", "charts")
GENRE_DIR = "./genres"
GENRE_LIST = ["classical", "jazz", "country", "pop", "rock", "metal"]

genre_list = GENRE_LIST


def read_fft(genre_list, base_dir=GENRE_DIR):
  X = []
  y = []
  for label, genre in enumerate(genre_list):
    genre_dir = os.path.join(base_dir, genre, genre + "*.fft.npy")
    # file_list = glob.glob(genre_dir) assert(file_list), genre_dir
    file_list = glob.glob(genre_dir)

    for fn in file_list:
      fft_features = np.load(fn)

      X.append(fft_features[:2000])
      y.append(label)

  return np.array(X), np.array(y)


def train_model(clf_factory, X, Y, name, plot=False):
  labels = np.unique(Y)

  cv = ShuffleSplit(
    n=len(X), n_iter=1, test_size=0.3, random_state=0)

  train_errors = []
  test_errors = []

  scores = []
  pr_scores = defaultdict(list)
  precisions, recalls, thresholds = defaultdict(
    list), defaultdict(list), defaultdict(list)

  roc_scores = defaultdict(list)
  tprs = defaultdict(list)
  fprs = defaultdict(list)

  clfs = []  # just to later get the median

  cms = []

  for train, test in cv:
    X_train, y_train = X[train], Y[train]
    X_test, y_test = X[test], Y[test]

    clf = clf_factory()
    clf.fit(X_train, y_train)
    clfs.append(clf)

    train_score = clf.score(X_train, y_train)
    test_score = clf.score(X_test, y_test)
    scores.append(test_score)

    train_errors.append(1 - train_score)
    test_errors.append(1 - test_score)

    y_pred = clf.predict(X_test)
    cm = confusion_matrix(y_test, y_pred)
    cms.append(cm)

    for label in labels:
      y_label_test = np.asarray(y_test == label, dtype=int)
      proba = clf.predict_proba(X_test)
      proba_label = proba[:, label]

      precision, recall, pr_thresholds = precision_recall_curve(
        y_label_test, proba_label)
      pr_scores[label].append(auc(recall, precision))
      precisions[label].append(precision)
      recalls[label].append(recall)
      thresholds[label].append(pr_thresholds)

      fpr, tpr, roc_thresholds = roc_curve(y_label_test, proba_label)
      roc_scores[label].append(auc(fpr, tpr))
      tprs[label].append(tpr)
      fprs[label].append(fpr)

  if plot:
    for label in labels:
      print("Plotting", genre_list[label])
      scores_to_sort = roc_scores[label]
      median = np.argsort(scores_to_sort)[len(scores_to_sort) / 2]

      desc = "%s %s" % (name, genre_list[label])
      plot_roc(roc_scores[label][median], desc, tprs[label][median],
               fprs[label][median], label='%s vs rest' % genre_list[label])

  all_pr_scores = np.asarray(pr_scores.values()).flatten()
  summary = (np.mean(scores), np.std(scores),
             np.mean(all_pr_scores), np.std(all_pr_scores))
  print("%.3f\t%.3f\t%.3f\t%.3f\t" % summary)

  return np.mean(train_errors), np.mean(test_errors), np.asarray(cms)


def create_model():
  from sklearn.linear_model.logistic import LogisticRegression
  clf = LogisticRegression()

  return clf


def plot_roc(auc_score, name, tpr, fpr, label=None):
  pylab.clf()
  pylab.figure(num=None, figsize=(5, 4))
  pylab.grid(True)
  pylab.plot([0, 1], [0, 1], 'k--')
  pylab.plot(fpr, tpr)
  pylab.fill_between(fpr, tpr, alpha=0.5)
  pylab.xlim([0.0, 1.0])
  pylab.ylim([0.0, 1.0])
  pylab.xlabel('False Positive Rate')
  pylab.ylabel('True Positive Rate')
  pylab.title('ROC curve (AUC = %0.2f) / %s' %
              (auc_score, label), verticalalignment="bottom")
  pylab.legend(loc="lower right")
  filename = name.replace(" ", "_")
  pylab.savefig(
    os.path.join(CHART_DIR, "roc_" + filename + ".png"), bbox_inches="tight")


def plot_confusion_matrix(cm, genre_list, name, title):
  pylab.clf()
  pylab.matshow(cm, fignum=False, cmap='Blues', vmin=0, vmax=1.0)
  ax = pylab.axes()
  ax.set_xticks(range(len(genre_list)))
  ax.set_xticklabels(genre_list)
  ax.xaxis.set_ticks_position("bottom")
  ax.set_yticks(range(len(genre_list)))
  ax.set_yticklabels(genre_list)
  pylab.title(title)
  pylab.colorbar()
  pylab.grid(False)
  pylab.show()
  pylab.xlabel('Predicted class')
  pylab.ylabel('True class')
  pylab.grid(False)
  pylab.savefig(
    os.path.join(CHART_DIR, "confusion_matrix_%s.png" % name), bbox_inches="tight")


if __name__ == "__main__":
  # X, y = read_ceps(genre_list)
  X, y = read_fft(GENRE_LIST, GENRE_DIR)

  train_avg, test_avg, cms = train_model(
    create_model, X, y, "Log Reg CEPS", plot=True)

  cm_avg = np.mean(cms, axis=0)
  cm_norm = cm_avg / np.sum(cm_avg, axis=0)

  print(cm_norm)

  plot_confusion_matrix(cm_norm, genre_list, "ceps",
                        "Confusion matrix of a CEPS based classifier")
