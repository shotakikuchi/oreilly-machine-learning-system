import numpy as np
import os, sys
import scipy
from scipy import io
from scipy.io import wavfile


def write_fft(fft_features, fn):
  """
  Write the FFT features to separate files to speed up processing.
  """
  base_fn, ext = os.path.splitext(fn)
  data_fn = base_fn + ".fft"

  np.save(data_fn, fft_features)
  print("Written", data_fn)


def create_fft(fn):
  sample_rate, X = scipy.io.wavfile.read(fn)

  fft_features = abs(scipy.fft(X)[:1000])
  write_fft(fft_features, fn)


genre_list = ["blues", "classical", "country", "jazz", "metal", "rock"]
for genre in genre_list:
  for i in range(10):
    file_name = './genres/%s/%s.0000%d.wav' % (genre, genre, i)
    create_name = './genres/%s/%s.0000%d.fft.npy' % (genre, genre, i)

    if not os.path.isfile(file_name):
      continue

    if os.path.isfile(create_name):
      continue
    create_fft(file_name)

  for i in range(10, 100):
    file_name = './genres/%s/%s.000%d.wav' % (genre, genre, i)
    create_name = './genres/%s/%s.0000%d.fft.npy' % (genre, genre, i)

    if not os.path.isfile(file_name):
      continue

    if os.path.isfile(create_name):
      continue

    create_fft(file_name)

#file_name = "./genres/classical/classical.00000.wav"

